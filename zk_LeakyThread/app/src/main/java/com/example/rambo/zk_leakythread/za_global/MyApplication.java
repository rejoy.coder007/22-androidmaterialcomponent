package com.example.rambo.zk_leakythread.za_global;

import android.app.Application;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.squareup.leakcanary.LeakCanary;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyApplication extends Application {

    private static MyApplication mContext;






    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        Log.d("_FRAG_READY", "onCreate: ");

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
        // Normal app init code...


    }

    public static MyApplication getContext() {
        return mContext;
    }



}

