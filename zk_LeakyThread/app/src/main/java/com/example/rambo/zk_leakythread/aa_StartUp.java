package com.example.rambo.zk_leakythread;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.rambo.zk_leakythread.za_global.MyApplication;

public class aa_StartUp extends AppCompatActivity {

    private FirstTimeThread firstTimeThread = new FirstTimeThread();
    public  static aa_StartUp aaStartUp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if(isFirstTime())
        {
            setContentView(R.layout.aa_startup);
            FirstTimeScreen();
        }
        else
        {

        }


        aa_StartUp.aaStartUp = this;

    }

    private boolean isFirstTime() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        boolean ranBefore = preferences.getBoolean("Permission", false);
        Log.d("FIRST_START", "isFirstTime: " + ranBefore);
        return !ranBefore;
    }


    private void FirstTimeScreen() {
        Log.d("#_Thread_1", "FirstTimeScreen:S ");
        firstTimeThread.start();
        Log.d("#_Thread_2", "FirstTimeScreen:E ");
    }

    private static void redirectToNewScreen() {
        Log.d("#_Thread_3", "Redirect:S ");
        aa_StartUp.aaStartUp.startActivity(new Intent(MyApplication.getContext(), Main1Activity.class));
        Log.d("#_Thread_4", "Redirect:E : ");


    }

    @Override
    protected void onPause() {
        super.onPause();


        Log.d("#ON_PAUSE", "onPause: ");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // FIX II: kill the thread
        Log.d("#_Thread_5", "Destroy:E : ");
        firstTimeThread.interrupt();
        Log.d("#_Thread_6", "Destroy:E : ");
    }


    /*
     * Fix III: Make thread static
     * */
    private static class FirstTimeThread extends Thread {
        @Override
        public void run() {

            Log.d("#_Thread_7", " Inside Thread : ");

          //  int i=0;
            //while (!isInterrupted()) {

             //   Log.d("#_DANGER", " Inside Thread : "+i++);

          //  }
            //

            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            redirectToNewScreen();

        }
    }

}
