package com.example.rambo.zh_c_tab_leak.za_global;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;


public class MyApplication extends Application {

    private static MyApplication mContext;

    public static MyApplication getContext() {
        return mContext;
    }


    @Override
    public void onCreate() {
        super.onCreate();


        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
        // Normal app init code...


    }


}

