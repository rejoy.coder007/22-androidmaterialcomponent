package com.example.rambo.myapplication;

import android.app.Application;
import android.util.Log;

 ;
import com.squareup.leakcanary.LeakCanary;

public class MyApplication extends Application {


    private static MyApplication mContext;



    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        Log.d("_FRAG_READY", "onCreate: ");

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
        // Normal app init code...


    }

    public static MyApplication getContext() {
        return mContext;
    }



}

