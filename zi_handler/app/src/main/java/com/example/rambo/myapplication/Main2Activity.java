package com.example.rambo.myapplication;

import android.app.ActivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import java.util.List;

public class Main2Activity extends AppCompatActivity {
    protected static ActivityManager activityManager;
    private static final String TAG ="#_SIMPLE_WORKER_2";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        if(activityManager==null){
            activityManager=(ActivityManager)getSystemService(ACTIVITY_SERVICE);
        }



    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)   {




        Log.d("hello", "key");

        switch (event.getKeyCode())
        {
            case KeyEvent.KEYCODE_A:
            {
                //your Action code
                Log.d("hello",getAppTaskState());

                return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    protected static String getAppTaskState(){



        StringBuilder stringBuilder=new StringBuilder();
        int totalNumberOfTasks=activityManager.getRunningTasks(10).size();//Returns total number of tasks - stacks
        stringBuilder.append("\nTotal Number of Tasks: "+totalNumberOfTasks+"\n\n");

        List<ActivityManager.RunningTaskInfo> taskInfo =activityManager.getRunningTasks(10);//returns List of RunningTaskInfo - corresponding to tasks - stacks

        for(ActivityManager.RunningTaskInfo info:taskInfo){
            stringBuilder.append("Task Id: "+info.id+", Number of Activities : "+info.numActivities+"\n");//Number of Activities in task - stack

            // Display the root Activity of task-stack
            stringBuilder.append("TopActivity: "+info.topActivity.getClassName().
                    toString().replace("com.example.rambo.material_android.","")+"\n");

            // Display the top Activity of task-stack
            stringBuilder.append("BaseActivity:"+info.baseActivity.getClassName().
                    toString().replace("com.example.rambo.material_android.","")+"\n");
            stringBuilder.append("\n\n");
        }
        return stringBuilder.toString();
    }


}
