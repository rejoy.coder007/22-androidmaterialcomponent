 package com.example.rambo.myapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.Random;

 public class MainActivity extends AppCompatActivity {
    private SimpleWorker worker;
     private static final String TAG ="#_SIMPLE_WORKER";
    private TextView textView;
     Boolean bool_final=false;
     public BooVariable bv = null;


    private Handler handler= new Handler(Looper.getMainLooper()){

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

          //bv.setBoo(false);
            textView.setText(msg.obj.toString());
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        textView = (TextView) findViewById(R.id.tv_message);

        Intent intent=new Intent(MainActivity.this,Main3Activity.class);
        //  startActivity(new Intent(MainActivity.this, Main2Activity.class));

        startActivityForResult(intent, 2);//

/*
        worker = new SimpleWorker();
        worker.execute(()->{
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Message message = Message.obtain();
            message.obj= "Task 1 obtined";
           handler.sendMessage(message);
        }).execute(()->{

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Message message = Message.obtain();
            message.obj= "Task 2 obtined";
            handler.sendMessage(message);

        }).execute(()->{
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Message message = Message.obtain();
            message.obj= "Task 3 obtined";
            bool_final=true;
          //  message.obj= "a";

            handler.sendMessage(message);
        }).execute(()->{

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


          //  new MyTask(this).execute();



        });

*/

    }



     @Override
     protected void onResume() {
         super.onResume();




     }

     @Override
     protected void onPause() {
        // worker.quit();
       //  worker.interrupt();

         super.onPause();
        // ActivityCompat.finishAffinity(MainActivity.this);
         Log.d(TAG, "stopped");
        finish();
     }


     public void newtask() {


         super.onDestroy();


     }

     @Override
     protected void onActivityResult(int requestCode, int resultCode, Intent data)
     {
         super.onActivityResult(requestCode, resultCode, data);
         // check if the request code is same as what is passed  here it is 2
         if(requestCode==2)
         {
             //startActivity(new Intent(MainActivity.this, Main2Activity.class));
            // finish();
         }
     }

     private static class MyTask extends AsyncTask<Void, Void, String> {
         private final WeakReference<MainActivity> mActivityRef;
         public MyTask(MainActivity activity){
             mActivityRef = new WeakReference<>(activity);
         }

         @Override
         protected String doInBackground(final Void ... params) {
             // a methods takes very long time
             String result = "hello"+(int )(Math. random() * 50 + 1);
             return result;
         }

         @Override
         protected void onPostExecute( final String result ) {
             // continue what you are doing...
             if (mActivityRef.get() != null){
                 mActivityRef.get().textView.setText(result);

                 Intent intent = new Intent(  mActivityRef.get(), Main2Activity.class);

                 mActivityRef.get().startActivity(intent);


             }
         }
     }
 }



 /*

      Boolean bool=true;



         while(bool){
             //Log.d("hello", "onCreate: ");




             if(bool_final){
                 Intent intent = new Intent(MainActivity.this, Main2Activity.class);

                 this.startActivity(intent);

                 bool=false;
             }




         }

         Boolean bool1=true;

         if(bool1)
             finish();
  */